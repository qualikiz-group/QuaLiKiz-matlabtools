kc = 1;
name{kc} = 'dimx';                  kc=kc+1;% Number of radial or scan points
name{kc} = 'dimn';         kc=kc+1;% Number of wavenumbers
name{kc} = 'nions';                      kc=kc+1;% Number of ions in system

%Flag input and metadata
name{kc} = 'phys_meth';	            kc=kc+1;%  Flag for additional calculation (default 0)
name{kc} = 'coll_flag';	            kc=kc+1;%  Flag for collisionality (default 0)
name{kc} = 'write_primi';           kc=kc+1;%      Flag for writing primitive outputs (default 1)
name{kc} = 'rot_flag';	            kc=kc+1;%  Flag  for rotation (default 0)
name{kc} = 'verbose';	            kc=kc+1;%  Flag  for setting level of output verbosity
name{kc} = 'numsols';	            kc=kc+1;% Number of solutions requested
name{kc} = 'relacc1';	 	    kc=kc+1;%  Relative accuracy in 1D integrals
name{kc} = 'relacc2';	 	    kc=kc+1;%  Relative accuracy in 2D integrals
name{kc} = 'maxruns'; 	            kc=kc+1;%  Number of runs jumping directly to Newton between contour checks
name{kc} = 'maxpts';                     kc=kc+1;% Number of integrand evaluations done in 2D integral
name{kc} = 'timeout';                    kc=kc+1;% Upper time limit (s) beyond which solutions are not sought after at a given wavenumber and radius
name{kc} = 'ETGmult';                    kc=kc+1;% ETG multiplier (for testing)
name{kc} = 'collmult';                   kc=kc+1;% collisionality multiplier (for testing)

%Geometry input
name{kc} = 'kthetarhos';                 kc=kc+1;%  Wave spectrum input: Vector (dimn)
name{kc} = 'x';                          kc=kc+1;%  radial normalised coordinate (midplane average)
name{kc} = 'rho';                        kc=kc+1;%  normalized toroidal flux coordinate
name{kc} = 'Ro';		            kc=kc+1;% (m) Major radius. Radial profile due to Shafranov shift
name{kc} = 'Rmin';	                    kc=kc+1;% (m) Geometric minor radius. Assumed to be a midplane average at LCFS. Currently a profile but should probably be shifted to a scalar
name{kc} = 'Bo';		            kc=kc+1;% (T) Likely not very rigorous to use this sqrt(<B��>) for calculating the Larmor radius % quite close to <Bphi> in practice however 
name{kc} = 'q';               	    kc=kc+1;%  Vector (radial grid x(aa))
name{kc} = 'smag';            	    kc=kc+1;%  Vector (radial grid x(aa))  q is a flux surface quantity --> makes sense to consider s = rho/q dq/drho
name{kc} = 'alpha';            	    kc=kc+1;%  Vector (radial grid x(aa)) 

%Rotation input
name{kc} = 'Machtor';            	    kc=kc+1;%  Vector (radial grid x(aa)) 
name{kc} = 'Autor';            	    kc=kc+1;%  Vector (radial grid x(aa)) 
name{kc} = 'Machpar';            	    kc=kc+1;%  Vector (radial grid x(aa)) 
name{kc} = 'Aupar';            	    kc=kc+1;%  Vector (radial grid x(aa)) 
name{kc} = 'gammaE';            	    kc=kc+1;%  Vector (radial grid x(aa)) 

%Electron input
name{kc} = 'Te';       		    kc=kc+1;% (keV) Vector (radial grid x(aa))
name{kc} = 'ne';    		    kc=kc+1;% (10^19 m^-3) Vector (radial grid x(aa))
name{kc} = 'Ate';                        kc=kc+1;%  Vector (radial grid x(aa))
name{kc} = 'Ane';                	    kc=kc+1;%  Vector (radial grid x(aa))
name{kc} = 'typee';                    kc=kc+1;% Kinetic or adiabatic
name{kc} = 'anise';                      kc=kc+1;%  Tperp/Tpar at LFS
name{kc} = 'danisdre';                   kc=kc+1;%  d/dr(Tperp/Tpar) at LFS

%Ion inputs (can be for multiple species)
name{kc} = 'Ai';	            kc=kc+1;% Ion mass
name{kc} = 'Zi';     	    kc=kc+1;%  Ion charge
name{kc} = 'Ti';                kc=kc+1;% (keV) Vector (radial grid x(aa))
name{kc} = 'normni';             kc=kc+1;% ni/ne Vector (radial grid x(aa))
name{kc} = 'Ati';      	    kc=kc+1;%  Vector (radial grid x(aa))
name{kc} = 'Ani';                kc=kc+1;%  Vector (radial grid x(aa))  check calculation w.r.t. Qualikiz electroneutrality assumption
name{kc} = 'typei';           kc=kc+1;%  Kinetic, adiabatic, tracer
name{kc} = 'anisi';               kc=kc+1;% Tperp/Tpar at LFS
name{kc} = 'danisdri';            kc=kc+1;% d/dr(Tperp/Tpar) at LFS

%special flags
name{kc} = 'separateflux';            kc=kc+1;% d/dr(Tperp/Tpar) at LFS
name{kc} = 'simple_mpi_only';            kc=kc+1;% for MPI debugging
